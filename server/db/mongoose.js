var mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI || 'mongodb://35.227.51.11:27017/ProjectList');

module.exports = {mongoose};
